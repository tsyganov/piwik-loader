# Piwik API Site Loader

Loads sites from given DataLoaderInterface data source (CSVFileLoader is included)

## Installation

* Add dependency to your project via composer.json:

        "repositories": [
            {
                "type": "vcs",
                "url": "git@bitbucket.org:tsyganov/piwik-loader.git"
            }
        ],
        "require": {
            "tsyganov/piwik-loader": "dev-master"
        }

* Install dependency
* Use it

        $dataLoader = new CSVFileLoader();
        $dataLoader->setFilename('/path/to/csv/file');

        $loader = new SiteLoader();
        $loader->setSite('%your_piwik_installation_url%');
        $loader->setTokenAuth('%your_piwik_token_auth%');
        $loader->setDataLoader($dataLoader);
        $loader->load();

        $result = $loader->getResult(); // Gets an array of XML strings with results.

* Parse results and display them