<?php
namespace PiwikLoader;

use PiwikLoader\DataLoader\DataLoaderInterface;

class SiteLoader
{
    /** @var  string */
    protected $site;

    /** @var  string */
    protected $tokenAuth;

    /** @var  DataLoaderInterface */
    protected $dataLoader;

    /** @var  array */
    protected $result;

    public function setDataLoader(DataLoaderInterface $dataLoader)
    {
        $this->dataLoader = $dataLoader;
    }

    public function load()
    {
        $data = $this->dataLoader->getData();

        foreach ($data as $site) {
            $requestBuilder = new RequestBuilder();
            $requestBuilder->setBaseUrl($this->site);
            $requestBuilder->setTokenAuth($this->tokenAuth);
            $requestBuilder->addCommand('SitesManager.addSite', $site);

            $httpClient = new HttpClient();
            $httpClient->setRequestUrl($requestBuilder->getRequestUrl());
            $this->result[] = $httpClient->getResponse();
        }

    }

    public function setSite($site)
    {
        $this->site = $site;
    }

    public function setTokenAuth($tokenAuth)
    {
        $this->tokenAuth = $tokenAuth;
    }

    public function getResult()
    {
        return $this->result;
    }
}