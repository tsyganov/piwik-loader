<?php

namespace PiwikLoader;

class HttpClient
{
    protected $requestUrl;

    public function setRequestUrl($requestUrl)
    {
        $this->requestUrl = $requestUrl;
    }

    public function getResponse()
    {
        return file_get_contents($this->requestUrl);
    }
}