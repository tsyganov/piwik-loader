<?php
namespace PiwikLoader\DataLoader;

interface DataLoaderInterface
{
    public function getData();
}