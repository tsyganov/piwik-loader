<?php
namespace PiwikLoader\DataLoader;

use PiwikLoader\DataLoader\Exception\FileDoesNotExistException;

class CSVFileLoader implements DataLoaderInterface
{
    protected $filename;

    protected $delimiter = ',';

    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    public function getData()
    {
        if(!file_exists($this->filename) || !is_readable($this->filename)) {
            throw new FileDoesNotExistException();
        }

        $handle = fopen($this->filename, 'r');
        $names = $this->getCsvRow($handle);
        $data = array();

        while (($row = $this->getCsvRow($handle)) !== false) {
            $data[] = array_combine($names, $row);
        }

        fclose($handle);

        return $data;
    }

    /**
     * @param $handle
     * @return array
     */
    public function getCsvRow($handle)
    {
        return fgetcsv($handle, null, $this->delimiter);
    }

    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }
}