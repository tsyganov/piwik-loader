<?php

namespace PiwikLoader;

class RequestBuilder
{
    protected $baseUrl;

    protected $commands;

    private $tokenAuth;

    private $querySeparator = '&';

    private $queryDelimiter = '?';

    private $apiMethod = 'API';

    private $bulkMethod = 'API.getBulkRequest';

    private $format = 'xml';

    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    public function setFormat($format)
    {
        $this->format = $format;
    }

    public function setTokenAuth($tokenAuth)
    {
        $this->tokenAuth = $tokenAuth;
    }

    public function getRequestUrl()
    {
        return $this->baseUrl . $this->queryDelimiter . http_build_query($this->getCommonParameters()) . $this->getQuery();
    }

    public function addCommand($command, $parameters = array())
    {
        $this->commands[] = array(
            'command' => $command,
            'parameters' => $parameters
        );
    }

    /**
     * @return array
     */
    protected function getCommonParameters()
    {
        $parameters = array(
            'module' => $this->apiMethod,
            'format' => $this->format,
        );

        if ($this->tokenAuth) {
            $parameters = $parameters + array('token_auth' => $this->tokenAuth);
        }

        return $parameters;
    }

    /**
     * @param $method
     * @param $parameters
     * @return string
     */
    protected function buildQuery($method, $parameters)
    {
        return http_build_query(array_merge(array('method' => $method), $parameters));
    }

    /**
     * @return string
     */
    protected function getQuery()
    {
        if (sizeof($this->commands) == 1) {

            $command = $this->commands[0];

            return $this->querySeparator . $this->buildQuery($command['command'], $command['parameters']);

        } elseif (sizeof($this->commands) > 1) {

            $urls = array();

            foreach ($this->commands as $command) {
                $urls[] = urlencode($this->buildQuery($command['command'], $command['parameters']));
            }

            return $this->querySeparator . $this->buildQuery($this->bulkMethod, array('urls' => $urls));
        }

        return '';
    }
}