<?php

use PiwikLoader\RequestBuilder;

class RequestBuilderTest extends PHPUnit_Framework_TestCase
{
    public function testRequestBuilder()
    {
        $requestBuilder = new RequestBuilder();

        $this->assertInstanceOf('PiwikLoader\RequestBuilder', $requestBuilder);
    }

    public function testGetRequestUrlWithBaseUrl()
    {
        $requestBuilder = new RequestBuilder();
        $requestBuilder->setBaseUrl('http://piwik.local/');

        $requestUrl = $requestBuilder->getRequestUrl();

        $this->assertEquals('http://piwik.local/?module=API&format=xml', $requestUrl);
    }

    public function testSetFormat()
    {
        $requestBuilder = new RequestBuilder();
        $requestBuilder->setBaseUrl('http://piwik.local/');
        $requestBuilder->setFormat('json');

        $requestUrl = $requestBuilder->getRequestUrl();
        $this->assertEquals('http://piwik.local/?module=API&format=json', $requestUrl);
    }

    public function testAddCommand()
    {
        $requestBuilder = new RequestBuilder();
        $requestBuilder->setBaseUrl('http://piwik.local/');
        $requestBuilder->addCommand('SitesManager.addSite');

        $requestUrl = $requestBuilder->getRequestUrl();

        $this->assertEquals('http://piwik.local/?module=API&format=xml&method=SitesManager.addSite', $requestUrl);
    }

    public function testAddCommandWithParameters()
    {
        $requestBuilder = new RequestBuilder();
        $requestBuilder->setBaseUrl('http://piwik.local/');

        $urls = array('http://somesite.com/', 'http://somesite-alias.com/');

        $parameters = array(
            'siteName' => 'Some Site',
            'urls' => $urls
        );

        $requestBuilder->addCommand('SitesManager.addSite', $parameters);

        $requestUrl = $requestBuilder->getRequestUrl();

        $urlData = parse_url($requestUrl);

        $this->assertEquals('http', $urlData['scheme']);
        $this->assertEquals('piwik.local', $urlData['host']);

        $requestData = array();
        parse_str($urlData['query'], $requestData);

        $this->assertEquals('SitesManager.addSite', $requestData['method']);
        $this->assertEquals('Some Site', $requestData['siteName']);
        $this->assertEquals($urls, $requestData['urls']);
    }

    public function testAddMultipleCommands()
    {
        $requestBuilder = new RequestBuilder();
        $requestBuilder->setBaseUrl('http://piwik.local/');
        $requestBuilder->addCommand('SitesManager.addSite');
        $requestBuilder->addCommand('SitesManager.getAllSites');

        $urlData = parse_url($requestBuilder->getRequestUrl());

        $requestData = array();
        parse_str($urlData['query'], $requestData);

        $this->assertEquals('API.getBulkRequest', $requestData['method']);
        $this->assertEquals(2, count($requestData['urls']));

        $url_0_Data = array();
        parse_str(urldecode($requestData['urls'][0]), $url_0_Data);

        $url_1_Data = array();
        parse_str(urldecode($requestData['urls'][1]), $url_1_Data);

        $this->assertEquals('SitesManager.addSite', $url_0_Data['method']);
        $this->assertEquals('SitesManager.getAllSites', $url_1_Data['method']);

    }

}