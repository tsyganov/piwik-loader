<?php

use PiwikLoader\DataLoader\CSVFileLoader;

class CSVFileLoaderTest extends PHPUnit_Framework_TestCase
{
    protected function getCSVFilename($filename)
    {
        return realpath(__DIR__ . '/static/' . $filename);
    }

    public function testCSVLoader()
    {
        $loader = new CSVFileLoader();

        $this->assertInstanceOf('PiwikLoader\DataLoader\CSVFileLoader', $loader);
    }

    public function testSetFilename()
    {
        $loader = new CSVFileLoader();
        $loader->setFilename($this->getCSVFilename('testData.csv'));

        $this->assertTrue(method_exists($loader, 'setFilename'));
    }

    /**
     * @expectedException PiwikLoader\DataLoader\Exception\FileDoesNotExistException
     */
    public function testSetNotExistingFile()
    {
        $loader = new CSVFileLoader();
        $loader->setFilename('not_existing_file.csv');

        $loader->getData();
    }

    public function testGetData()
    {
        $loader = new CSVFileLoader();

        $loader->setFilename($this->getCSVFilename('testData.csv'));

        $data = $loader->getData();

        $this->assertEquals('Value1', $data[0]['paramName1']);
        $this->assertEquals('Value2', $data[0]['paramName2']);
        $this->assertEquals('Value3', $data[1]['paramName1']);
        $this->assertEquals('Value4', $data[1]['paramName2']);

    }

    public function testSetDelimiter()
    {
        $loader = new CSVFileLoader();
        $loader->setFilename($this->getCSVFilename('testDataSemicolon.csv'));
        $loader->setDelimiter(';');

        $data = $loader->getData();

        $this->assertEquals('value1', $data[0]['param1']);
        $this->assertEquals('value2', $data[0]['param2']);

    }

}