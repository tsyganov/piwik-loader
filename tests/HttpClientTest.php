<?php

use PiwikLoader\HttpClient;

class HttpClientTest extends PHPUnit_Framework_TestCase
{
    public function testHttpClient()
    {
        $httpClient = new HttpClient();

        $this->assertInstanceOf('PiwikLoader\HttpClient', $httpClient);
    }

    public function testSetUrlAndGetResponse()
    {
        $httpClient = new HttpClient();
        $httpClient->setRequestUrl(__DIR__ . '/static/sitesData.csv');

        $response = $httpClient->getResponse();

        $this->assertGreaterThan(0, strlen($response));
    }
}