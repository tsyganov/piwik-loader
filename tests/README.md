# How to run tests

## Unit tests

User phpunit.xml to run test suite, or specify `bootstrap` parameter

    phpunit --bootstrap=vendor/autoload.php

## Functional (depend on working piwik installation)

In order to run skipped functional tests that are skipped by default, you need to copy `functional.ini.dist` file to
`functional.ini` and configure all parameters inside. Then, just run test suite again.

    cp functional.ini.dist functional.ini

Parameters:

* `piwik_url` - URL to your (local) installation of Piwik. Beware, test WILL create sites there (starting with
  `TestSite_`
* `piwik_token_auth` - Auth token for your Piwik user.