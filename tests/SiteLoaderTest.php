<?php

use PiwikLoader\DataLoader\CSVFileLoader;
use PiwikLoader\DataLoader\DataLoaderInterface;
use PiwikLoader\SiteLoader;

class TestDataLoader implements DataLoaderInterface {

    public function getData()
    {
        $siteNumber = substr(md5(microtime() . mt_rand()), 0, 5);

        return array(
            array(
                'siteName' => 'TestSite_' . $siteNumber,
                'urls' => 'http://testsite-' . $siteNumber . '.local/'
            )
        );
    }
}

class SiteLoaderTest extends PHPUnit_Framework_TestCase
{
    protected $parameters;

    protected function setUp()
    {
        //$this->markTestSkipped('Skip for now');

        $filename = __DIR__ . '/functional.ini';

        if (!file_exists($filename) or !is_readable($filename)) {
            $this->markTestSkipped(
                'Functional parameters are not specified. Skipping.'
            );
        }

        $this->parameters = parse_ini_file($filename);
    }

    public function testSiteLoader()
    {
        $loader = new SiteLoader();

        $dataLoader = new TestDataLoader();

        $loader->setSite($this->parameters['piwik_url']);
        $loader->setTokenAuth($this->parameters['piwik_token_auth']);
        $loader->setDataLoader($dataLoader);
        $loader->load();

        $result = $loader->getResult();

        $this->assertEquals(1, sizeof($result));

        /** @var SimpleXMLElement $resultItem */
        $resultItem = new SimpleXmlElement($result[0]);

        $this->assertEmpty($resultItem->error);
    }

}